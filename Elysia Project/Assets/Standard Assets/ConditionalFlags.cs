﻿using UnityEngine;
using System.Collections;

public class ConditionalFlags : MonoBehaviour {
	
	public bool Object01 = false;
	public bool Object02 = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void ItemCheckCondition1 () {
			if (Object01 == true && Object02 == true){
				print ("ItemCheck complete. Need to transmit response globally");
			}
	}
}
