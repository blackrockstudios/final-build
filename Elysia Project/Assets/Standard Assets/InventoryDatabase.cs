﻿using UnityEngine;
using System.Collections;

public class InventoryDatabase : MonoBehaviour {
//-----------------------//
//By Joseph Boddy
//Made 2/09/13
//Inventory Database
//Version 1
//-----------------------//

	
	//Connection to the Global Transmitter
	public GameObject Global;
	
	//Connection to Player Internal Script
	public GameObject Player;
	
	//Items and their respective Inventory Slot Image
	
	//Object 1
	public bool object01 = false;
	//public GameObject InventorySlot1;
	
	//Object 2
	public bool object02 = false;
	//public GameObject InventorySlot2;
	
	//Object 3
	public bool object03 = false;
	//public GameObject InventorySlot3;
	
	
	// Use this for initialization
	void Start () {
	DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Tab))
		{	
			print ("Inventory!");
			InventoryCheck();
		}	
	}
	//Checks the Inventory Status
	void InventoryCheck () {
		if (object01 == true)
		{
			Player.BroadcastMessage ("Object1IsActive");
			print ("The body of the victim. He was stabbed multiple times in the chest and there are signs of blunt force trauma on the head...probably from when he fell.");
		}
		
		if (object02 == true)
		{
			Player.BroadcastMessage ("Object2IsActive");
			print ("How odd...This mark...it...It was carved here by the killer....but why?");
		}
		if (object03 == true)
		{
			Player.BroadcastMessage ("Object3IsActive");
			print (" This is where the victim fell....a lot of force must have been applied in order to break the banister like that...");
		}
	}
	
	//Checks the conditions of the inventory and sends a response.
	void ItemCheckCondition1 () {
		print ("Message Received. Processing your claim now.");
		if (object01 == true && object02 == true && object03 == true) {
			Global.BroadcastMessage ("Response1");
		}
		else {
			print ("Can't let you do that, Starfox.");		
		}
	}
	//Inventory Item Status
	void Object1 () {
		object01 = true;
	}
	
	void Object2 () {
		object02 = true;
	}
	
	void Object3 () {
		object03 = true;
	}
}
