﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	public Camera Camera1;//First Camera
	public Camera Camera2;//John Bio
	public Camera Camera3;//Daniel Bio
	public Camera Camera4;//Charlotte Bio
	public Camera Camera5;//Jacob Bio
	public Camera Camera6;//Serana Bio
	public Camera Camera7;//Kevin Bio
	public Camera Camera8;//John Reveal
	public Camera Camera9;//Daniel Reveal
	public Camera Camera10;//Charlotte Reveal
	public Camera Camera11;//Jacob Reveal
	public Camera Camera12;//Serava Reveal
	public Camera Camera13;//Kevin Reveal
	//public Camera Camera14;
	
	// Use this for initialization
	void Start () {
	Camera1.enabled = true;
	Camera2.enabled = false;
	Camera3.enabled = false;
	Camera4.enabled = false;
	Camera5.enabled = false;
	Camera6.enabled = false;
	Camera7.enabled = false;
	Camera8.enabled = false;
	Camera9.enabled = false;
	Camera10.enabled = false;
	Camera11.enabled = false;
	Camera12.enabled = false;
	Camera13.enabled = false;
	//Camera14.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void Cam1(){
		Camera1.enabled = true;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam2(){
		Camera1.enabled = false;
		Camera2.enabled = true;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam3(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = true;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam4(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = true;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam5(){
		Camera1.enabled = true;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = true;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam6(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = true;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam7(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = true;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam8(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = true;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam9(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = true;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam10(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = true;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam11(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = true;
		Camera12.enabled = false;
		Camera13.enabled = false;
	}
	void Cam12(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = true;
		Camera13.enabled = false;
	}
	void Cam13(){
		Camera1.enabled = false;
		Camera2.enabled = false;
		Camera3.enabled = false;
		Camera4.enabled = false;
		Camera5.enabled = false;
		Camera6.enabled = false;
		Camera7.enabled = false;
		Camera8.enabled = false;
		Camera9.enabled = false;
		Camera10.enabled = false;
		Camera11.enabled = false;
		Camera12.enabled = false;
		Camera13.enabled = true;
	}
}
